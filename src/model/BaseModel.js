export function send(method, url, success, fail) {
  var request = new XMLHttpRequest();
  request.open(method, url, true);
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      var data = JSON.parse(request.responseText);
      success(data);
    } else {
      if(fail) fail(request)
    }
  };
  request.onerror = fail;
  request.send();
}
