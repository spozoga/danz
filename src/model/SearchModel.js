import * as base from "./BaseModel";

export class SearchModel {

  constructor(context) {
    this.baseurl = context.config.apiurl;
    this.apikey = context.config.apikey;
  }

  search(query) {
    var url = this.baseurl + '/3/search/movie?api_key=' + this.apikey + '&query=' + encodeURIComponent(query);
    return new Promise(function(resolve, reject) {
      base.send('GET', url, resolve, reject);
    });
  }

}
