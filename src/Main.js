import { MainController } from "./MainController";
import * as config from "./Config";

class Main {

  bootstrap() {
    // execution context make possible to mock a dom elements
    var context = {
      window: window,
      document: document,
      body: document.getElementsByTagName("body")[0],
      $searchComponent: document.getElementById('search-by'),
      $listView: document.getElementById('list'),
      config: config
    };
    var controller = new MainController(context);
    controller.run();
  }
}

(function(){
  var main = new Main();
  main.bootstrap();
})();
