
export class SearchComponent {

  constructor($container) {
    this.$container = $container;
  }

  bind(onChange) {
    this.$container.addEventListener('change', e => {
      onChange(e.target.value);
    });
  }

}
