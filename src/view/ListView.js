
export class ListView {

  constructor(config, $container) {
    this.config = config;
    this.$container = $container;
  }

  render(data) {
    var list = document.createElement('ol');
    for(var i=0; i<data.length; i++) {
      var row = this.renderItem(data[i]);
      list.append(row);
    }
    while(this.$container.firstChild) {
        this.$container.removeChild(this.$container.firstChild);
    }
    this.$container.appendChild(list);
  }

  renderItem(data) {
    var row = document.createElement('li');

    var a = document.createElement('a');
    a.href = '#';
    row.appendChild(a);

    var coverSmall = document.createElement('img');
    coverSmall.src = this.config.imageurl + data.poster_path;
    a.appendChild(coverSmall);

    var content = document.createElement('div');
    row.appendChild(content);

    var title = document.createElement('div');
    title.appendChild(document.createTextNode(data.title));
    title.className = 'title';
    content.appendChild(title);

    var hr = document.createElement('hr');
    content.appendChild(hr);

    var overview = document.createElement('div');
    overview.appendChild(document.createTextNode(data.overview));
    overview.className = 'overview';
    content.appendChild(overview);

    var releaseDate = document.createElement('div');
    releaseDate.appendChild(document.createTextNode(data.release_date));
    releaseDate.className = 'release';
    content.appendChild(releaseDate);

    var popularity = document.createElement('div');
    popularity.appendChild(document.createTextNode(data.popularity.toFixed(2)));
    popularity.className = 'popularity';
    content.appendChild(popularity);

    var link = document.createElement('a');
    link.href = this.config.moviewurl + data.id;
    link.appendChild(document.createTextNode('Go to themoviedb'));
    link.className = 'link';
    content.appendChild(link);

    var clr = document.createElement('div');
    clr.className = 'clr';
    row.appendChild(clr);

    return row;
  }

}
