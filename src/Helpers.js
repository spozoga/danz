
export function dispachEvent($elem, eventName) {
  var event;
  if ($elem.createEvent) {
    event = $elem.createEvent("HTMLEvents");
    event.initEvent(eventName, true, true);
  } else {
    event = $elem.createEventObject();
    event.eventType = eventName;
  }
  event.eventName = eventName;
  if ($elem.createEvent) {
    $elem.dispatchEvent(event);
  } else {
    $elem.fireEvent("on" + event.eventType, event);
  }
}
