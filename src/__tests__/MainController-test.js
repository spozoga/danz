import MainController from '../MainController';
import * as config from '../Config';
import * as helpers from '../Helpers';

describe('MainController', () => {
  it('should show result list after insert search criteria', () => {
    /*
     * There is no time to write a unit tests.
     * I wirited simple integration test (Top-Down test).
     */

    // prepare mockup state
    var $search = document.createElement('input');
    var $list = document.createElement('div');

    var context = {
      window: null,
      document: document,
      body: $mocupDOM,
      $searchComponent: $search,
      $listView: $list,
      config: config
    };
    console.log('context', context);
    var controller = new MainController(context);
    controller.run();

    // run test sequence
    $search.value = 'The Witcher 3: Wild Hunt concert';
    helpers.dispachEvent($search, 'change');

    // check result
    setTimeout(() => {
      // the timeout is a little simplification
      var hasWildHunt = $mocupDOM.innerText.indexOf('The Witcher 3: Wild Hunt concert');
      expect(hasWildHunt).toBe(true);
    }, 1000);
  });
});
