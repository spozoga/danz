import { SearchModel } from "./model/SearchModel";
import { SearchComponent } from "./view/SearchComponent";
import { ListView } from "./view/ListView";


export class MainController {

  constructor(context) {
    this.context = context;
    this.searchModel = new SearchModel(context);
    this.searchComponent = new SearchComponent(context.$searchComponent);
    this.listView = new ListView(context.config, context.$listView);
  }

  run() {
    this.searchComponent.bind(this.onSearchChange.bind(this));
    this.listView.render([]);
  }

  onSearchChange(query) {
    this.searchModel.search(query).then(data => this.renderList(data.results));
  }

  renderList(data) {
    this.listView.render(data);
  }

}
