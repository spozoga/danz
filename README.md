# Run app

To build and run app use
```
npm run start
```

To run test run
```
npm run test
```

## Devtools
To use prepared developer environment [try](https://github.com/sebastianpozoga/docker-desktop-devtools)
